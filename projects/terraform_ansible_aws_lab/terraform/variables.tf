

# security_group ports
# add or remove ports from default
variable "sg_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [80, 22, 443]
}

variable "region" {
  default = "us-west-1"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "name_tags" {
  type    = list(any)
  default = ["ansible_controller", "ansible-target1", "ansible-target2"]
}



# Don't change these here
# Don't give these defaults
variable "AWS_ACCESS_KEY" {
  type        = string
  description = "set these as environment variables or wait for input prompt."
}

variable "AWS_SECRET_KEY" {
  type        = string
  description = "set these as environment variables.."
}
# more info on Environment Variables: https://www.terraform.io/language/values/variables
# Terraform searches the environment for variables named TF_VAR_ followed by the name of a declared variable.
# in ~/.bashrc add: export TF_VAR_AWS_ACCESS_KEY="<value>"


variable "project_name" {
  type    = string
  default = "terraform_ansible"
}


locals {
  user            = "ubuntu"
  key_name        = "${var.project_name}-ssh-key"
  ssh_public_key  = "~/.ssh/aws/keys/${var.project_name}-ssh-key.pub"
  ssh_private_key = "~/.ssh/aws/keys/${var.project_name}-ssh-key.pem"
}
