#!/bin/bash

remote_ip='159.223.182.150'
dir_name='docker'

# rsync -e "ssh -i ~/.ssh/digitalocean/keys/my_lab_server_key-ssh-key.pem -o StrictHostKeyChecking=no" \
#     -raz --progress \
#     ~/code/devops/${dir_name}/ "root@${remote_ip}:${dir_name}/"


ssh -i ~/.ssh/digitalocean/keys/my_lab_server_key-ssh-key.pem root@$remote_ip -o StrictHostKeyChecking=no <<HERE



    remote_ip='159.223.182.150'
    dir_name='docker'


    # install docker https://docs.docker.com/engine/install/ubuntu/
    apt-get update


    apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release


    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg


    echo \
      "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


    apt-get update
    apt-get install docker-ce docker-ce-cli containerd.io

    systemctl status docker --no-pager
    docker info

    pwd
    ls
    cd docker
    pwd
    ls
    cd

HERE
