#!/bin/bash

project_name='my_lab_server_key'

mkdir -p ~/.ssh/digitalocean/keys/
cd ~/.ssh/digitalocean/keys/
ssh-keygen -P "" -t rsa -b 4096 -m pem -f ${project_name}-ssh-key
mv ${project_name}-ssh-key ${project_name}-ssh-key.pem
chmod 400 ${project_name}-ssh-key.pem

