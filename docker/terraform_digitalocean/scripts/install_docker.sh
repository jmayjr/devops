#!/bin/bash

# remote_ip='159.223.182.150'
# dir_name='docker'


# install docker https://docs.docker.com/engine/install/ubuntu/
apt-get update && apt-get -y upgrade


apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release


curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

y

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


apt-get update
apt-get -y install docker-ce docker-ce-cli containerd.io

systemctl status docker --no-pager
docker info


# install docker-compose
apt-get install -y docker-compose
docker-compose --version
