terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}


# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.DIGITALOCEAN_TOKEN
}


# note* my-instance-name is valid my_instance_name was not

# Create a new Web Droplet in the nyc2 region
resource "digitalocean_droplet" "my-instance-name" {
  image  = "ubuntu-20-04-x64"
  name   = "docker-lab"
  region = "nyc1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
}



data "digitalocean_ssh_key" "terraform" {
  name = "my_lab_server_key"
  # public_key = file("~/.ssh/digitalocean/keys/terraform-ssh-key.pub")
}

# ssh -i ~/.ssh/digitalocean/keys/terraform-ssh-key.pem root@<instance_ip>
