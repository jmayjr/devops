# healthcheck

https://docs.docker.com/engine/reference/run/#healthcheck



to run a container with a health check add `HEALTHCHECK` to the Dockerfile

```bash
docker container run -dt --name busybox busybox sh

docker build -t monitor healthcheck/
docker container run -dt --name monitor monitor sh
```
