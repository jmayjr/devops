
# DevOps

Get familiar with these skills / tools

- Iac
    * [x] terrraform

- Configuration Management
    * [ ] ansible

- CI/CD
    * [x] Gitlab
    * [x] Github

- Test
    * [ ] Selenium
    * [x] Django Test
    * [x] Doctest, Pytest, Unittest

- Containerization
    * [x] Docker
    * [ ] Kubernetes

- Monitor
    * [ ]
    * [x] AWS (CloudWatch, CloudTrail)

- Security
    * [ ]
