# Terraform

ensure you have terraform installed
```bash
terraform -version
```

[install instuctions](https://learn.hashicorp.com/tutorials/terraform/install-cli)

for Ubuntu install

```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
```



## setup credentials with environment variables

Terraform and aws look for keys in different ways,
you can use the same keys for both or have different keys for the backend.
I used the same keys with both formats.

```bash
# terrform:
export TF_VAR_AWS_ACCESS_KEY="key1_CHANGE_ME"
export TF_VAR_AWS_SECRET_KEY="key2_CHANGE_ME"

# aws:
export AWS_ACCESS_KEY="key1_CHANGE_ME"
export AWS_SECRET_KEY="key2_CHANGE_ME"
```

##  setup ssh

```bash
chmod +x scripts/setup_ssh.sh
project_name='terraform_ansible' scripts/setup_ssh.sh
```



##  run Terraform

from the `terraform_aws` directory

```bash
terraform init
terraform plan
```

if the plan looks good run `terraform apply`
```bash
terraform apply
```
