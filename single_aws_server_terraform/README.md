# Single AWS server


This will provision a single AWS Ubuntu server.



# Terraform

Ensure you have Terraform installed
```bash
terraform -version
```

[Install Instructions](https://learn.hashicorp.com/tutorials/terraform/install-cli)

For Ubuntu install

```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install Terraform
```

## setup credentials with environment variables

Terraform and AWS look for keys in different ways,
you can use the same keys for both or have different keys for the backend.
I used the same keys with both formats.

```bash
# terrform:
export TF_VAR_AWS_ACCESS_KEY="key1_CHANGE_ME"
export TF_VAR_AWS_SECRET_KEY="key2_CHANGE_ME"

# aws:
export AWS_ACCESS_KEY="key1_CHANGE_ME"
export AWS_SECRET_KEY="key2_CHANGE_ME"
```

##  Setup SSH

```bash
chmod +x scripts/setup_ssh.sh
project_name='single_server' scripts/setup_ssh.sh
```



##  Run Terraform

from the Terraform directory

```bash
terraform init
terraform plan
```

If the plan looks good run `terraform apply`.
```bash
terraform apply
```
when prompted type `yes`

To destroy the server run `terraform destroy`.
```bash
terraform destroy
```
