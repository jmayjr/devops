
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region     = var.region
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}


resource "aws_instance" "my_instance_name" {
  # ami                    = "ami-01f87c43e618bf8f0"
  ami                    = data.aws_ami.ubuntu_ami.id # latest ubuntu
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  count                  = 1

  tags = {
    Name = element(var.name_tags, count.index)
  }

}

variable "key_name"{
    default = "single_server"
}


variable "instance_type" {
  default = "t2.micro"
}

variable "name_tags" {
  type    = list(any)
  default = ["my_test_server"]
}

output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.my_instance_name[*].id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.my_instance_name[*].public_ip
}

variable "region" {
  default = "us-west-1"
}

variable "AWS_ACCESS_KEY" {
  type        = string
  description = "This is an example input variable using env variables."
}

variable "AWS_SECRET_KEY" {
  type        = string
  description = "This is another example input variable using env variables."
}


# dynamic Blocks
# https://www.terraform.io/language/expressions/dynamic-blocks
# You can dynamically construct repeatable nested blocks
variable "sg_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [80, 22, 443]
}

resource "aws_security_group" "my_security_group" {
  name        = "my_single_server_security_group"
  description = "Ingress for my_security_group"

  dynamic "ingress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = var.sg_ports
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

# Data Source: ubuntu_ami
# Use this data source to get the ID of a registered AMI for use in other resources.
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami
data "aws_ami" "ubuntu_ami" {
  most_recent = true
  owners      = ["099720109477"] # Canonical


  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
# ami = data.aws_ami.ubuntu_ami.id

output "ubuntu_name" {
  value = data.aws_ami.ubuntu_ami.name
}
